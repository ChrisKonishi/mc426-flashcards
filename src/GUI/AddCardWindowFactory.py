from GUI.AddCard import AddCard
from GUI.AddStandardCard import AddStandardCard
from abc import ABC, abstractmethod


class AddCardWindowFactory(ABC):

    @abstractmethod
    def createWindow(self) -> AddCard:
        pass

class StandardCardWindowFactory(AddCardWindowFactory):

    def createWindow(self) -> AddCard:
    	window = AddStandardCard()
    	return window
