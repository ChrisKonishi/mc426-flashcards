from PyQt5 import uic, QtWidgets, QtCore
from deck.deck import Deck
import os

class DeleteDeckConfirmation(QtWidgets.QMainWindow):
    def __init__(self, mainMenu):
        super().__init__()
        self.mainMenu = mainMenu
        self.dir_path = os.path.dirname(os.path.realpath(__file__))
        self.tela = uic.loadUi("./GUI/designs/DeleteDeckConfirmation.ui")
        self.tela.setWindowFlags(QtCore.Qt.WindowCloseButtonHint | QtCore.Qt.WindowMinimizeButtonHint)
        self.tela.confirmButton.clicked.connect(self.__confirm)
        self.tela.cancelButton.clicked.connect(self.__cancel)

    def show(self):
        self.tela.show()

    def __confirm(self):
        deck_id, _ = self.mainMenu.getDeckSelectedIdName()
        deck = Deck(deck_id)
        deck.delDeck()
        self.mainMenu.updateDeckVisualization()
        self.tela.close()
    
    def __cancel(self):
        self.tela.close()
