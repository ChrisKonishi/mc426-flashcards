import os

from PyQt5 import uic, QtWidgets
from PyQt5.QtCore import Qt

from GUI.AddCardMenu import AddCardMenu
from GUI.AddDeckMenu import AddDeckMenu
from GUI.RenameDeckMenu import RenameDeckMenu
from GUI.CardView import CardView
from GUI.DeleteDeckConfirmation import DeleteDeckConfirmation

from deck.deck import Deck


class MainWindow():
    def __init__(self):
        app = QtWidgets.QApplication([])

        self.window = uic.loadUi("./GUI/designs/mainMenu.ui")
        self.__loadButtons()
        self.__prepareDeckVisualization()
        self.window.show()
        app.exec()

    def __loadButtons(self):
        self.window.addCardButton.clicked.connect(self.__addCardPopup)
        self.window.addDeckButton.clicked.connect(self.__addDeckPopup)
        self.window.rename_button.hide()
        self.window.rename_button.clicked.connect(self.__renamePopup)
        self.window.delete_button.hide()
        self.window.delete_button.clicked.connect(self.__deletePopup)

    def __prepareDeckVisualization(self):
        self.updateDeckVisualization()
        self.window.deckList.itemDoubleClicked.connect(self.__deckClicked)
        self.window.deckList.itemSelectionChanged.connect(self.__itemSelected)

    def __addCardPopup(self):
        self.addMenu = AddCardMenu()
        self.addMenu.showAddCardMenu()

    def __addDeckPopup(self):
        self.addDeck = AddDeckMenu(self)
        self.addDeck.showAddDeckMenu()

    def updateDeckVisualization(self):
        win = self.window
        deckView = self.window.deckList
        self.deckList = Deck.getDeckList()  # deck dictionary list
        deckNames = list(map(lambda x: x["name"], self.deckList))
        deckView.clear()

        for i in deckNames:
            item = QtWidgets.QListWidgetItem(i)
            # item.setTextAlignment(Qt.AlignHCenter)
            deckView.addItem(item)

        deckView.sortItems()
        
        self.window.rename_button.hide()
        self.window.delete_button.hide()

        if deckView.count() == 0:
            win.label.setText("Nenhum deck encontrado!")
        else:
            win.label.setText("Lista de Decks")

    def __deckClicked(self):
        deckView = self.window.deckList
        deckName = deckView.selectedItems()[0].text()
        idDeck = Deck.getDeckByName(deckName)

        self.cardView = CardView(idDeck) 
        self.cardView.showCardView() #Abre a visualização dos cards do deck selecionado

        print(idDeck, deckName)

    
    def __itemSelected(self):
        self.window.delete_button.show()
        self.window.rename_button.show()

    def getDeckSelectedIdName(self):
        deckView = self.window.deckList
        print(deckView.selectedItems())

        if deckView.selectedItems() != []:
            deckName = deckView.selectedItems()[0].text()
            idDeck = Deck.getDeckByName(deckName)

            return idDeck, deckName

        else:
            return None, None


    def __renamePopup(self):
        self.rename_deck = RenameDeckMenu(self)
        self.rename_deck.showAddDeckMenu()


    def __deletePopup(self):
        self.delete_deck = DeleteDeckConfirmation(self)
        self.delete_deck.show()