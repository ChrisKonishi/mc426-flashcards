from PyQt5 import uic, QtWidgets, QtCore
from deck.deck import Deck
import os
from GUI.AddDeckMenu import AddDeckMenu

class RenameDeckMenu(AddDeckMenu):
    def __init__(self, mainMenu):
        super().__init__(mainMenu)
        self.Tela.pushButton_2.setText("Renomear")

    def createDeckButton(self):
        deckName = self.Tela.lineEdit.text()
        if deckName == "":
            return

        if not(self._deckExists(deckName)): #check if deck exists

            deck_id, _ = self.mainMenu.getDeckSelectedIdName()
            deck = Deck(deck_id)
            deck.editDeck(deckName)
            self.mainMenu.updateDeckVisualization()
            self.Tela.close()

        else:
            self.Tela.label_2.show()