import sqlite3


class DataManager():

    # Initializes the database
    # Input: None
    # Output: None
    def __init__(self):
        self.conn = sqlite3.connect('flashcards.db')
        self.cursor = self.conn.cursor()
        self.cursor.execute("PRAGMA foreign_keys = ON;")
        self.cursor.execute(
            """CREATE TABLE IF NOT EXISTS decks (
            deckid INTEGER PRIMARY KEY,
            name TEXT NOT NULL
            );
            """
        )
        self.cursor.execute(
            """CREATE TABLE IF NOT EXISTS cards (
            cardid INTEGER PRIMARY KEY,
            tip TEXT NOT NULL,
            ans TEXT NOT NULL
            );
            """
        )
        self.cursor.execute(
            """CREATE TABLE IF NOT EXISTS contains (
            deckid INTEGER,
            cardid INTEGER,
            PRIMARY KEY(deckid,cardid),
            FOREIGN KEY(deckid) REFERENCES decks(deckid)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
            FOREIGN KEY(cardid) REFERENCES cards(cardid)
                ON DELETE CASCADE
                ON UPDATE CASCADE
            );
            """
        )
        self.conn.commit()

    # Purges the whole database and then rebuilds the tables
    # Input: None
    # Output: None
    def purgeDatabase(self):
        self.cursor.execute('DROP TABLE IF EXISTS contains;')
        self.cursor.execute('DROP TABLE IF EXISTS cards;')
        self.cursor.execute('DROP TABLE IF EXISTS decks;')
        self.__init__()

    # Inserts a new deck entry
    # Input: New deck's name
    # Output: The id of the deck that was just created
    def insertNewDeck(self, name):
        self.cursor.execute("SELECT * FROM decks WHERE name = (:name)",{'name':name})
        if self.cursor.fetchall() == []:
            self.cursor.execute(
                "INSERT INTO decks VALUES (null, :name);", {'name': name})
            self.conn.commit()
            return self.cursor.lastrowid
        return None

    # Inserts a new card to an existent deck
    # Input: Front (tip) ans back (ans) of a flashcard, and the deck id that the new card will be inserted into
    # Output: The id of the card that was just created
    def insertNewCard(self, tip, ans, deckid):
        self.cursor.execute(
            "SELECT * FROM decks WHERE deckid = (:deck_id)", {'deck_id': deckid})
        if self.cursor.fetchall() != []:
            self.cursor.execute("INSERT INTO cards VALUES (null, :tip, :ans);", {
                                'tip': tip, 'ans': ans})
            cardid = self.cursor.lastrowid
            self.cursor.execute("INSERT INTO contains VALUES (:deckid, :cardid);", {
                                'deckid': deckid, 'cardid': cardid})
            self.conn.commit()
            return cardid
        else:
            return None

    # Gets all tuples from a table in the database
    # Input: The name of a database
    # Output: A list of tuples that are in the table
    ### This is prone to SQL injection attacks. Use it just for debugging purposes ###
    def _getTable(self, tableName):
        try:
            self.cursor.execute("SELECT * FROM {}".format(tableName))
            return self.cursor.fetchall()
        except:
            print('No table with the name {}'.format(tableName))

    # Gets all card ids from a deck
    # Input: The decks id
    # Output: A list of "incomplete" tuples, where each tuple has a card id of a card that is in the deck corresponding to the deckid
    def getCardsFromDeck(self, deckid):
        try:
            self.cursor.execute(
                "SELECT contains.cardid FROM contains WHERE contains.deckid = (:deck)", {'deck': deckid})
            return self.cursor.fetchall()
        except:
            print('No deck with id: {}'.format(deckid))

    # Get all decks and names
    # Input: None
    # Output: A list of tuples, where each tuple contains an id and a name corresponding to each deck
    def getDecks(self):
        try:
            self.cursor.execute("SELECT decks.deckid,decks.name FROM decks")
            return self.cursor.fetchall()
        except:
            print('Nothing found :(')

    # Get the cards front and back corresponding to a card id
    # Input: A cards id
    # Output: A list with containing a tuple containing the front and back of the card
    def getCard(self, cardid):
        try:
            self.cursor.execute(
                "SELECT cards.tip,cards.ans FROM cards WHERE cards.cardid = (:id_card)", {'id_card': cardid})
            return self.cursor.fetchall()
        except:
            print('No card with id: {}'.format(cardid))

    # Edits a cards front or back
    # Input: A cards id, the new front (can be None) and the new back (can also be None)
    # Output: True if all changes could have been made and false otherwise
    def editCard(self, cardid, newtip, newans=None):
        try:
            if newtip != None:
                self.cursor.execute("UPDATE cards SET tip = (:newtip) WHERE cardid = (:id_card)", {
                                    'newtip': newtip, 'id_card': cardid})
            if newans != None:
                self.cursor.execute("UPDATE cards SET ans = (:newans) WHERE cardid = (:id_card)", {
                                    'newans': newans, 'id_card': cardid})
            self.conn.commit()
            return True
        except:
            print('No card with id: {}'.format(cardid))
            return False

    # Deletes a deck and all of its cards
    # Input: A decks id to be deleted
    # Ouput: None
    def deleteDeck(self, deckid):
        cards = self.getCardsFromDeck(deckid)
        for card in cards:
            self.cursor.execute("DELETE FROM cards WHERE cardid = (:card_id)", {
                                'card_id': card[0]})
        self.cursor.execute("DELETE FROM decks WHERE deckid = (:deck_id)", {
                            'deck_id': deckid})
        self.conn.commit()

    # Edits a decks name
    # Input: The decks id that is going to be renamed and the new name
    # Output: None
    def editDeck(self, deckid, newName):
        self.cursor.execute("UPDATE decks SET name = (:new_name) WHERE deckid = (:deck_id)", {
                            'new_name': newName, 'deck_id': deckid})
        self.conn.commit()

    # Copies a card to a new deck ("deep copy" - creates an independent card that is an exact copy of the older one)
    # Input: A cards id to be copied and a decks that will receive the copy
    # Output: The id of the deck that was just copied
    def copyCard(self, present_cardid, copy_deckid):
        card = self.getCard(present_cardid)
        if card != []:
            return self.insertNewCard(card[0][0], card[0][1], copy_deckid)
        return None
